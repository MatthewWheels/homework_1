//
//  PlanetClass.cpp
//  Planets Project
//
//  Created by Matthew Wheeler on 1/10/20.
//

#include "PlanetClass.hpp"






void PlanetClass::setup(){
     width = ofGetWidth();
     height = ofGetHeight();
    radiansX = 0;
    radiansY= 0;
    amp = ofRandom(20, 360);
    rotRate = ofRandom(0.01, 0.02);
    red = ofRandom(255);
    green = ofRandom(255);
    blue = ofRandom(255);
    planetSize = ofRandom(10, 100);
        std::cout<<amp<<"Amp:"<<endl;
}

void PlanetClass::update(){
    planetX = width/2+sin(radiansX)*amp;
    planetY = height/2+cos(radiansY)*amp;
    radiansX += rotRate;
    radiansY += rotRate;
     std::cout<<planetX<<"PlanetX:"<<endl;
    
}


void PlanetClass::draw(){
    ofSetColor(red, green, blue);
    ofDrawEllipse(planetX, planetY, planetSize, planetSize);
    
}
