#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

      //---------------------------TRANSLATION-----------------------------------
//    xz=0;
//   xz1=0;
//   xz2=0;
//   xz3=0;
//   width=1024;
//   height=768;
    
    //---------------------------ARRAY-----------------------------------
        for(int i=1; i<nPlanets; i++){
        Planet[i].setup();
     }
//
    //---------------------------MULTIPLE INSTANCES-----------------------------------
    
//       planet.setup();
//    planet1.setup();
//       planet2.setup();
//       planet3.setup();
//       planet4.setup();
//       planet5.setup();
//       planet6.setup();
//       planet7.setup();
//    planet8.setup();
    
    
     }

//--------------------------------------------------------------
void ofApp::update(){

      //---------------------------TRANSLATION-----------------------------------
//    x=width/2+sin(xz)*20;
//       x1=width/2+sin(xz2)*160;
//       x2=width/2+sin(xz1)*320;
//       y=height/2+cos(xz)*20;
//       yy=height/2+cos(xz2)*160;
//       y2=height/2+cos(xz1)*320;
//      x3=width/2+sin(xz3)*90;
//    y3=height/2+cos(xz3)*90;
//     xz+=0.1;
//     xz1+=0.01;
//     xz2+=0.2;
//     xz3+=0.05;

    
//---------------------------ARRAY-----------------------------------
    
    for(int i=1; i<nPlanets; i++){
          Planet[i].update();
      }
    
//---------------------------MULTIPLE INSTANCES-----------------------------------
    
//    planet.update();
//    planet1.update();
//    planet2.update();
//    planet3.update();
//    planet4.update();
//    planet5.update();
//    planet6.update();
//    planet7.update();
//    planet8.update(); 

}

//--------------------------------------------------------------
void ofApp::draw(){

//      //---------------------------TRANSLATION-----------------------------------
//       ofSetColor(200, 32, 100);
//        ofDrawEllipse(x,y,20,20);
//       ofSetColor(150, 100, 32);
//       ofDrawEllipse(x1,yy,20,20);
//       ofSetColor(32, 200, 100);
//        ofDrawEllipse(x2,y2,20,20);
//       ofSetColor(100, 32, 200);
//       ofDrawEllipse(x3, y3, 20, 20);
    
    //---------------------------ARRAY-----------------------------------
    
    for(int i=1; i<nPlanets; i++){
        Planet[i].draw();
    }

    //---------------------------MULTIPLE INSTANCES-----------------------------------
    
//    planet.draw();
//    planet1.draw();
//    planet2.draw();
//    planet3.draw();
//    planet4.draw();
//    planet5.draw();
//    planet6.draw();
//    planet7.draw();
//    planet8.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
   
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
