//
//  PlanetClass.hpp
//  Planets Project
//
//  Created by Matthew Wheeler on 1/10/20.
//

#ifndef PlanetClass_hpp
#define PlanetClass_hpp

#include <ofMain.h>

class PlanetClass {
public:
    void setup();
    void update();
    void draw();
    float planetX, planetY, radiansX, radiansY, planetSize, amp, rotRate, width, height, red, green, blue;
};
#endif /* PlanetClass_hpp */
