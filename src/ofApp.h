#pragma once

#include "ofMain.h"
#include "PlanetClass.hpp"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
       //---------------------------TRANSLATION-----------------------------------
    
//            float x, y, x1, yy, x2, y2, x3, y3, xz, xz1, xz2, xz3, width, height;
    
       //---------------------------MULTIPLE INSTANCES-----------------------------------
    
//    PlanetClass planet, planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8;

    
      //---------------------------ARRAY-----------------------------------
    #define nPlanets 9
    PlanetClass Planet[];
};
